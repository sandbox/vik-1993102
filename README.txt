========================
TurboSMS (Drupal Module)
======================== 

DESCRIPTION
----------- 
TurboSMS is a service for the rapid and effective warning to your customers,
partners and employees by sending sms. Website - http://turbosms.ua/.
Localization service TurboSMS only Russian. Module facilitates communication
between Drupal and SMS gateway TurboSMS.

INSTALLATION
------------
1. Create folder 'sites/all/modules/turbosms' and copy all the modules files,
   keeping directory structure, to this folder.
2. If updating, run the update.php script following the standard procedure for
   Drupal updates.
3. Setting module.   
   a. Register in the system http://turbosms.ua/. Add the sender's signature.
      In the section "Connecting to the gateway" set up a login and password
	  gateway, select connection method - SQL. 10 test sms with registration
	  are provided free.
   b. Specify in the settings.php settings for connecting to a server table
      TurboSMS. Example:

	  $databases = array (
        'default' => 
        array (
          'default' => 
          array (
	        'database' => 'your_database',
      	    'username' => 'your_username',
      	    'password' => 'your_password',
      	    'host' => 'localhost',
     	    'port' => '',
      	    'driver' => 'mysql',
      	    'prefix' => '',
    	  ),
  	    ),
        'turbosms' => 
        array (
          'default' => 
          array (
            'database' => 'users', 
            'username' => 'your_login_gateway',    // login gateway
            'password' => 'your_password_gateway', // password gateway
	        'collation' => 'utf8_general_ci',
            'host' => '77.120.116.10',
            'port' => '',
            'driver' => 'mysql',
            'prefix' => '',
	        'pdo' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
          ),
        ),
      ); 	  

4. Enable the TurboSMS module in the administration tools.

CREDIT
------
Modul TurboSMS was developed and is maintained by Vitaliy Sytnik
<http://v.sytnik.lviv.ua>.
