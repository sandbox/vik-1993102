<?php

/**
 * @file
 * Page callbacks for informing, adding, editing, and deleting management for
 * content.
 */

/**
 * Form builder; The sms info form.
 *
 * @param array $node
 *   The array sms object.
 *
 * @ingroup forms
 * @see turbosms_page_info_submit()
 */
function turbosms_page_info($form, &$form_state, $node) {
  $form = array();

  $form['turbosms_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Info SMS'),
  );

  $form['turbosms_info']['sid'] = array(
    '#type' => 'item',
    '#title' => t('Id messages : ') . $node->sid,
  );
  $form['turbosms_info']['msg_id'] = array(
    '#type' => 'item',
    '#title' => t('ID messages in the system : ') . ($node->msg_id ? $node->msg_id : t('is empty')),
  );
  $form['turbosms_info']['number'] = array(
    '#type' => 'item',
    '#title' => t('Number of the recipient : ') . $node->number,
  );
  $form['turbosms_info']['sign'] = array(
    '#type' => 'item',
    '#title' => t('Signature or number of the sender : ') . $node->sign,
  );
  $form['turbosms_info']['message'] = array(
    '#type' => 'item',
    '#title' => t('Text messages : ') . $node->message,
  );
  $form['turbosms_info']['wappush'] = array(
    '#type' => 'item',
    '#title' => t('Link WAPPush : ') . ($node->wappush ? $node->wappush : t('is empty')),
  );
  $form['turbosms_info']['cost'] = array(
    '#type' => 'item',
    '#title' => t('The message will cost in credits system : ') . ($node->cost ? $node->cost : t('is empty')),
  );
  $form['turbosms_info']['credits'] = array(
    '#type' => 'item',
    '#title' => t('The balance of credits to the account of the user : ') . ($node->credits ? $node->credits : t('is empty')),
  );

  $format = 'Y-m-d H:i';
  $date = ($node->send_time ? format_date($node->send_time, 'custom', $format) : t('is empty'));
  $form['turbosms_info']['send_time'] = array(
    '#type' => 'item',
    '#title' => t('Date and time of the scheduled message sending : ') . $date,
  );
  $date = ($node->sended ? format_date($node->sended, 'custom', $format) : t('is empty'));
  $form['turbosms_info']['sended'] = array(
    '#type' => 'item',
    '#title' => t('Date and time of the actual message sending : ') . $date,
  );
  $date = ($node->updated ? format_date($node->updated, 'custom', $format) : t('is empty'));
  $form['turbosms_info']['updated'] = array(
    '#type' => 'item',
    '#title' => t('Date and time of last data update messages : ') . $date,
  );

  $form['turbosms_info']['status'] = array(
    '#type' => 'item',
    '#title' => t('Text description the result sending : ') . ($node->status ? $node->status : t('is empty')),
  );
  $form['turbosms_info']['dlr_status'] = array(
    '#type' => 'item',
    '#title' => t('The delivery status of the protocol specification SMPP v3.4 + a few of our values : ') . ($node->dlr_status ? $node->dlr_status : t('is empty')),
  );

  $form['turbosms_info']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('turbosms_page_edit_submit'),
  );

  return $form;
}

/**
 * Form submission handler for turbosms_page_info().
 *
 * @see turbosms_page_info()
 */
function turbosms_page_info_submit($form, &$form_state) {
  $form_state['redirect'] = array('admin/turbosms/messages');
}

/**
 * Form builder; The sms add form.
 *
 * @ingroup forms
 * @see turbosms_page_add_submit() 
 */
function turbosms_page_add($form, &$form_state) {
  $form = array();

  $active = array(
    0 => t('Immediately'),
    1 => t('At the specified time'),
  );

  $form['turbosms_add'] = array(
    '#type' => 'fieldset',
    '#title' => t('New SMS'),
  );

  $num_active = !empty($form_state['values']['time']['turbosms_add']) ? $form_state['values']['time']['turbosms_add'] : 0;

  $form['turbosms_add']['time'] = array(
    '#type' => 'radios',
    '#title' => t('Time of sending'),
    '#default_value' => $num_active,
    '#options' => $active,
    '#weight' => -10,
    '#ajax' => array(
      'callback' => 'turbosms_time_ajax_callback',
      'wrapper' => 'date-div',
      'effect' => 'none',
      'progress' => array('type' => 'none'),
    ),
  );

  $date = time('Y-m-d H:i');
  $format = 'Y-m-d H:i';

  $form['turbosms_add']['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Date and Time'),
    '#disabled' => ($num_active == 0 ? TRUE : FALSE),
    '#description' => t('Date and time of the planned message sending. Date and Time format: YYYY-MM-DD HH:MM'),
    '#default_value' => format_date($date, 'custom', $format),
    '#required' => TRUE,
    '#weight' => -9,
    '#prefix' => '<div id="date-div">',
    '#suffix' => '</div>',
  );

  $form['turbosms_add']['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Telephone'),
    '#description' => t('Number of the recipient is specified in international format.'),
    '#default_value' => '+',
    '#size' => 13,
    '#maxlength' => 13,
    '#required' => TRUE,
    '#weight' => -8,
  );

  $form['turbosms_add']['sign'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#description' => t('Signature or number of the sender.'),
    '#size' => 13,
    '#maxlength' => 13,
    '#required' => TRUE,
    '#weight' => -7,
  );

  $form['turbosms_add']['message'] = array(
    '#title' => t('Text messages'),
    '#type' => 'textarea',
    '#description' => t('1 SMS - 160 Latin characters or 70 Cyrillic characters.'),
    '#required' => TRUE,
    '#weight' => -6,
  );

  $form['turbosms_add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('turbosms_page_add_submit'),
  );

  return $form;
}

/**
 * Form submission handler for turbosms_page_add().
 *
 * @see turbosms_page_add()
 */
function turbosms_page_add_submit($form, &$form_state) {
  $sids = db_insert('turbosms')
    ->fields(array(
      'number' => $form_state['values']['number'],
      'sign' => $form_state['values']['sign'],
      'message' => $form_state['values']['message'],
      'send_time' => ($form_state['values']['date'] ? strtotime($form_state['values']['date']) : NULL),
    ))
    ->execute();

  if ($sids) {
    drupal_set_message(t('The add SMS.'));
  }

  // cache_clear_all();
  $form_state['redirect'] = 'admin/turbosms/messages';
}

/**
 * Callback element.
 *
 * @return array
 *   renderable array (the radios fieldset)
 * @see turbosms_page_add() 
 * @see turbosms_page_edit()
 */
function turbosms_time_ajax_callback($form, $form_state) {
  return $form['turbosms_add']['date'];
}

/**
 * Form builder; The sms edit form.
 *
 * @param array $node
 *   The array sms object.
 * 
 * @ingroup forms
 * @see turbosms_page_edit_submit() 
 */
function turbosms_page_edit($form, &$form_state, $node) {
  $form = array();

  if ($node->dlr_status == '') {
    $active = array(
      0 => 'Immediately',
      1 => 'At the specified time',
    );

    $form['turbosms_add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Edit SMS'),
    );
    $form['turbosms_add']['sid'] = array(
      '#type' => 'token',
      '#default_value' => $node->sid,
    );

    $num_active = (!empty($form_state['values']['time']['turbosms_add']) ? $form_state['values']['time']['turbosms_add'] : 1);

    $form['turbosms_add']['time'] = array(
      '#type' => 'radios',
      '#title' => t('Time of sending'),
      '#default_value' => $num_active,
      '#options' => $active,
      '#weight' => -10,
      '#ajax' => array(
        'callback' => 'turbosms_time_ajax_callback',
        'wrapper' => 'date-div',
        'effect' => 'none',
        'progress' => array('type' => 'none'),
      ),
    );

    $format = 'Y-m-d H:i';

    $form['turbosms_add']['date'] = array(
      '#type' => 'textfield',
      '#title' => t('Date and Time'),
      '#disabled' => ($num_active == 0 ? TRUE : FALSE),
      '#description' => t('Date and time of the planned message sending. Date and Time format: YYYY-MM-DD HH:MM'),
      '#default_value' => format_date($node->send_time, 'custom', $format),
      '#required' => TRUE,
      '#weight' => -9,
      '#prefix' => '<div id="date-div">',
      '#suffix' => '</div>',
    );

    $form['turbosms_add']['number'] = array(
      '#type' => 'textfield',
      '#title' => t('Telephone'),
      '#description' => t('Number of the recipient is specified in international format.'),
      '#default_value' => $node->number,
      '#size' => 13,
      '#maxlength' => 13,
      '#required' => TRUE,
      '#weight' => -8,
    );

    $form['turbosms_add']['sign'] = array(
      '#type' => 'textfield',
      '#title' => t('Sender'),
      '#description' => t('Signature or number of the sender.'),
      '#default_value' => $node->sign,
      '#size' => 13,
      '#maxlength' => 13,
      '#required' => TRUE,
      '#weight' => -7,
    );

    $form['turbosms_add']['message'] = array(
      '#title' => t('Text messages'),
      '#type' => 'textarea',
      '#description' => t('1 SMS - 160 Latin characters or 70 Cyrillic characters.'),
      '#default_value' => $node->message,
      '#required' => TRUE,
      '#weight' => -6,
    );

    $form['turbosms_add']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#submit' => array('turbosms_page_edit_submit'),
    );

    $form['turbosms_add']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('turbosms_page_edit_submit'),
    );
  }
  else {
    $form['turbosms_add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Can not be edited SMS'),
    );
  }

  return $form;
}

/**
 * Form submission handler for turbosms_page_edit().
 *
 * @see turbosms_page_edit()
 */
function turbosms_page_edit_submit($form, &$form_state) {
  if ($form_state['values']['op'] == 'Submit') {
    $node = turbosms_load($form_state['values']['sid']);
    turbosms_update($form_state['values']);
    watchdog('content', 'updated sms to @number message: %message.', array('@number' => $node->number, '%message' => $node->message));
    drupal_set_message(t('Sms "%message" has been updated.', array('%message' => $node->message)));
  }
  $form_state['redirect'] = array('admin/turbosms/messages');
}

/**
 * Form constructor for the sms deletion confirmation form.
 *
 * @param array $node
 *   The array sms object.
 * 
 * @see turbosms_delete_confirm_submit()
 */
function turbosms_delete_confirm($form, &$form_state, $node) {
  $form['#node'] = $node;

  $form['sid'] = array('#type' => 'value', '#value' => $node->sid);
  return confirm_form($form,
    t('Are you sure you want to delete sms?'),
    'admin/turbosms/messages/' . $node->sid,
    t('This action cannot be undone. Number: %number; message: %message',
      array('%number' => $node->number, '%message' => $node->message)),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Form submission handler for turbosms_delete_confirm().
 *
 * @see turbosms_delete_confirm()
 */
function turbosms_delete_confirm_submit($form, &$form_state) {
  global $databases;

  if ($form_state['values']['confirm']) {
    $node = turbosms_load($form_state['values']['sid']);
    turbosms_delete($form_state['values']['sid'], $databases['turbosms']['default']['username']);
    watchdog('content', 'deleted sms to @number message: %message.', array('@number' => $node->number, '%message' => $node->message));
    drupal_set_message(t('Sms "%message" has been deleted.', array('%message' => $node->message)));
  }

  $form_state['redirect'] = array('admin/turbosms/messages');
}
