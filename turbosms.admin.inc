<?php

/**
 * @file
 * The admin sms functions. 
 */

/**
 * Form builder; The gateway TurboSMS information form.
 *
 * @ingroup forms
 */
function turbosms_admin_gateway() {
  global $databases;
  $databases_default = variable_get('turbosms_database_default');
  $database = $databases['turbosms']['default']['database'];
  $username = $databases['turbosms']['default']['username'];
  $password = $databases['turbosms']['default']['password'];
  $host = $databases['turbosms']['default']['host'];
  $driver = $databases['turbosms']['default']['driver'];

  $form = array();

  $form['turbosms_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('TurboSMS information'),
  );
  $form['turbosms_info']['turbosms_markup_1'] = array(
    '#markup' => t('The database server is the IP address') . ' '
    . $databases_default['turbosms_databases']['host'] . t(', working database — «')
    . $databases_default['turbosms_databases']['database'] . t('», your name table is the same as and a login connect to the gateway.') . '<br/>',
    '#weight' => -10,
  );
  $form['turbosms_info']['turbosms_markup_2'] = array(
    '#markup' => t('Account created with limited access. You can write values ​​only in a specific field, and can also delete records. Change field values ​​or table structure, you can not. Handling recordings occurs every minute.'),
    '#weight' => -9,
  );

  $form['turbosms_information'] = array(
    '#type' => 'fieldset',
    '#title' => t('TurboSMS details'),
  );
  $form['turbosms_information']['turbosms_database'] = array(
    '#type' => 'item',
    '#title' => t('Working database'),
    '#markup' => ($database ? $database : 'NULL. Parameter is not set'),
  );
  $form['turbosms_information']['turbosms_username'] = array(
    '#type' => 'item',
    '#title' => t('User name table'),
    '#markup' => ($username ? $username : 'NULL. Parameter is not set'),
  );
  $form['turbosms_information']['turbosms_password'] = array(
    '#type' => 'item',
    '#title' => t('User password table'),
    '#markup' => ($password ? $password : 'NULL. Parameter is not set'),
  );
  $form['turbosms_information']['turbosms_host'] = array(
    '#type' => 'item',
    '#title' => t('Host'),
    '#markup' => ($host ? $host : 'NULL. Parameter is not set'),
  );
  $form['turbosms_information']['turbosms_driver'] = array(
    '#type' => 'item',
    '#title' => t('The type of database'),
    '#markup' => ($driver ? $driver : 'NULL. Parameter is not set'),
  );
  if (!empty($database) && !empty($username) && !empty($password) && !empty($host) && !empty($driver)) {
    db_set_active('turbosms');
    $query = db_select($username, 'n')
      ->fields('n', array('id', 'credits', 'dlr_status'));
    $id = $query->execute()->fetchAllAssoc('id');
    db_set_active('default');
    $id_count = count($id);

    $id_unsended = 0;
    $id_delete = array();
    foreach ($id as $key => $value) {
      foreach ($value as $k => $v) {
        if ($v == 'UNSENDED') {
          ++$id_unsended;
          array_push($id_delete, $key);
        }
      }
    }
    $id_delete = array_flip($id_delete);
    $id = array_diff_key($id, $id_delete);

    end($id);
    $id_max = current($id);
    $credits = $id_max->credits;

    $id_count_site = count(db_select('turbosms', 'n')->fields('n', array('sid'))->execute()->fetchCol());
    $id_count_site_new = (($id_count_site >= $id_count) ? $id_count_site - $id_count : NULL);
    $form['turbosms_configuration'] = array(
      '#type' => 'fieldset',
      '#title' => t('SMS configuration'),
    );
    $form['turbosms_configuration']['turbosms_markup_1'] = array(
      '#markup' => t('Server TurboSMS: recordings') . ' - ' . $id_count . t('; including untreated SMS - ') . $id_unsended . '; ' . t('the balance of credits') . ' - ' . $credits . '<br/>',
      '#weight' => -10,
    );
    if ($id_count_site_new) {
      $form['turbosms_configuration']['turbosms_markup_2'] = array(
        '#markup' => t('The Web Site: recordings') . ' - ' . $id_count_site . t('; including the new SMS - ') . $id_count_site_new . '<br/><br/>',
        '#weight' => -9,
      );
    }
    else {
      $form['turbosms_configuration']['turbosms_markup_2'] = array(
        '#markup' => t('The Web Site: recordings') . ' - ' . $id_count_site . '<br/><br/>',
        '#weight' => -9,
      );
    }
    $form['turbosms_configuration']['drop'] = array(
      '#type' => 'submit',
      '#value' => t('Drop base SMS web site'),
      '#submit' => array('turbosms_admin_gateway_drop_submit'),
    );
    $form['turbosms_configuration']['import'] = array(
      '#type' => 'submit',
      '#value' => t('Import database of TurboSMS'),
      '#submit' => array('turbosms_admin_gateway_import_submit'),
    );
  }
  return $form;
}

/**
 * Form submission handler for turbosms_admin_gateway().
 *
 * @see turbosms_admin_gateway()
 */
function turbosms_admin_gateway_drop_submit($form, &$form_state) {
  db_truncate('turbosms')->execute();
}

/**
 * Form submission handler for turbosms_admin_gateway().
 *
 * @see turbosms_admin_gateway()
 */
function turbosms_admin_gateway_import_submit($form, &$form_state) {
  global $databases;
  $username = $databases['turbosms']['default']['username'];
  db_set_active('turbosms');
  $sids = db_select($username, 'n')
    ->fields('n')
    ->execute()
    ->fetchAll();
  db_set_active('default');

  turbosms_batch_import_start($sids);
}

/**
 * Batch start function.
 */
function turbosms_batch_import_start($sids) {
  $batch = array(
    'operations' => array(
      array('_batch_turbosms_import', array($sids)),
    ),
    'finished' => 'turbosms_batch_finished',
    'title' => t('Import SMS of TurboSMS'),
    'progress_message' => t('Import. Operation @current out of @total.'),
    'error_message' => t('The import SMS of TurboSMS an error!'),
    'file' => drupal_get_path('module', 'turbosms') . '/turbosms.admin.inc',
  );
  batch_set($batch);
}

/**
 * Import from. 
 */
function _turbosms_batch_import($sids, &$context) {
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($sids);
    $context['sandbox']['sids'] = $sids;
    watchdog('import', 'import SMS');
  }
  // Process import SMS by groups of 5.
  $count = min(5, count($context['sandbox']['sids']));
  for ($i = 1; $i <= $count; $i++) {
    $sid = array_shift($context['sandbox']['sids']);

    db_insert('turbosms')
      ->fields(array(
        'sid' => $sid->id,
        'msg_id' => $sid->msg_id,
        'number' => $sid->number,
        'sign' => $sid->sign,
        'message' => $sid->message,
        'wappush' => $sid->wappush,
        'cost' => $sid->cost,
        'credits' => $sid->credits,
        'send_time' => ($sid->send_time ? strtotime($sid->send_time) : NULL),
        'sended' => ($sid->sended ? strtotime($sid->sended) : NULL),
        'updated' => ($sid->updated ? strtotime($sid->updated) : NULL),
        'status' => ($sid->status ? $sid->status : NULL),
        'dlr_status' => ($sid->dlr_status ? $sid->dlr_status : NULL),
        ))
      ->execute();

    // Store result for post-processing in the finished callback.
    $context['results'][] = $sid;
    $context['results']['added']['sms'][] = $sid->sid;

    // Update our progress information.
    $context['sandbox']['progress']++;

    // Messages.
    $context['message'] = t('Now processing import sms %name. Import %progress of %count',
      array(
        '%name' => $sid->sid,
        '%progress' => $context['sandbox']['progress'],
        '%count' => $context['sandbox']['max'],
      ));
    $context['results']['processed'] = $context['sandbox']['progress'];
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Finish of butch. Messagess.
 */
function turbosms_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('@count sms added.', array('@count' => isset($results['added']['sms']) ? count($results['added']['sms']) : 0)));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args',
      array(
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      )));
  }
  watchdog('import turbosms', 'import finished');
}

/**
 * Form builder; The sms administration form.
 *
 * @ingroup forms
 * @see turbosms_filter_form()
 * @see turbosms_admin_account()
 */
function turbosms_admin_messages() {
  $build = array();

  $build['filter'] = drupal_get_form('turbosms_filter_form');
  $build['admin'] = drupal_get_form('turbosms_admin_account');

  return $build;
}

/**
 * List sms administration filters that can be applied.
 *
 * @return array
 *   An associative array of filters.
 */
function turbosms_filters() {
  $filters['status'] = array(
    'title' => t('status'),
    'field' => 'n.dlr_status',
    'options' => array(
      '[any]' => t('any'),
      'NULL' => t('new sms'),
      'UNSENDED' => t('unsended'),
      'ERROR' => t('error'),
      'SENDED' => t('sended'),
      'DELIVRD' => t('delivered'),
      'OTHER' => t('other'),
    ),
  );
  return $filters;
}

/**
 * Form builder; Return form for sms administration filters.
 *
 * @ingroup forms
 * @see turbosms_filter_form_submit()
 */
function turbosms_filter_form() {
  $session = isset($_SESSION['turbosms_overview_filter']) ? $_SESSION['turbosms_overview_filter'] : array();
  $filters = turbosms_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__turbosms',
  );

  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $value = $filters[$type]['options'][$value];
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('%property is %value', $t_args));
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_library('system', 'drupal.form');

  $form['#submit'][] = 'turbosms_filter_form_submit';
  return $form;
}

/**
 * Form submission handler for turbosms_filter_form().
 *
 * @see turbosms_admin_messages()
 * @see turbosms_filters()
 */
function turbosms_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = turbosms_filters();
  switch ($op) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Merge an array of arrays into one if necessary.
          $options = $filters[$filter]['options'];
          // Only accept valid selections offered on the dropdown, block bad
          // input.
          if (isset($options[$form_state['values'][$filter]])) {
            $_SESSION['turbosms_overview_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;

    case t('Undo'):
      array_pop($_SESSION['turbosms_overview_filter']);
      break;

    case t('Reset'):
      $_SESSION['turbosms_overview_filter'] = array();
      break;

    case t('Update'):
      return;
  }
  $form_state['redirect'] = 'admin/turbosms/messages';
}

/**
 * Form builder; Administration page.
 *
 * @ingroup forms
 * @see turbosms_admin_messages()
 * @see turbosms_filter_form()
 * @see turbosms_filters() 
 */
function turbosms_admin_account() {
  // Build the sortable table header.
  $header = array(
    'sid' => array('data' => t('ID'), 'field' => 'n.sid', 'sort' => 'desc'),
    'number' => array('data' => t('Number'), 'field' => 'n.number'),
    'sign' => array('data' => t('Sing'), 'field' => 'n.sign'),
    'message' => array('data' => t('Message'), 'field' => 'n.message'),
    'cost' => array('data' => t('Cost'), 'field' => 'n.cost'),
    'send_time' => array('data' => t('Send time'), 'field' => 'n.send_time'),
    'status' => array('data' => t('Status'), 'field' => 'n.status'),
  );

  $filters = turbosms_filters();
  $query = db_select('turbosms', 'n');
  foreach (isset($_SESSION['turbosms_overview_filter']) ? $_SESSION['turbosms_overview_filter'] : array() as $filter) {
    list($key, $value) = $filter;
    switch ($value) {
      case 'NULL':
        $query->condition($filters[$key]['field'], NULL);
        break;

      case '[any]':
      case 'OTHER':
        $query
        ->condition(
          db_or()
            ->condition($filters[$key]['field'], 'REJECTD')
            ->condition($filters[$key]['field'], 'ACCEPTD')
            ->condition($filters[$key]['field'], 'UNKNOWN')
            ->condition($filters[$key]['field'], 'RECREDITED')
            ->condition($filters[$key]['field'], 'STOPPED')
            ->condition($filters[$key]['field'], 'REMOVED')
            ->condition($filters[$key]['field'], 'UNDELIV')
            ->condition($filters[$key]['field'], 'DELETED')
            ->condition($filters[$key]['field'], 'EXPIRED')
            ->condition($filters[$key]['field'], 'ENROUTE')
          );
        break;

      default:
        $query->condition($filters[$key]['field'], $value);
    }
  }

  $header['operations'] = array('data' => t('Operations'));

  $nids = $query
    ->fields('n', array('sid'))
    ->execute()
    ->fetchCol();

  if (count($nids) > 0) {
    $nodes = $query
      ->fields('n', array(
       'sid',
       'number',
       'sign',
       'message',
       'cost',
       'credits',
       'send_time',
       'status',
       'dlr_status',
       ))
      ->condition('n.sid', $nids, 'IN')
      ->extend('PagerDefault')
      ->limit(20)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->addTag('turbosms_access')
      ->execute();
  }
  else {
    $nodes = $query->execute();
  };

  $destination = drupal_get_destination();
  $options = array();
  foreach ($nodes as $node) {
    $options[$node->sid] = array(
      'sid' => $node->sid,
      'number' => $node->number,
      'sign' => $node->sign,
      'message' => $node->message,
      'cost' => $node->cost,
      'send_time' => ($node->send_time ? format_date($node->send_time, 'custom', 'Y-m-d h:i') : ''),
      'status' => $node->status,
    );

    // Build a list of all the accessible operations for the current sms.
    $operations = array();
    $operations['info'] = array(
      'title' => t('info'),
      'href' => 'admin/turbosms/messages/' . $node->sid . '/info',
      'query' => $destination,
    );
    if (time() < ($node->send_time && $node->dlr_status == '')) {
      // ($node->dlr_status == '' || $node->dlr_status == 'UNSENDED').
      $operations['edit'] = array(
        'title' => t('edit'),
        'href' => 'admin/turbosms/messages/' . $node->sid . '/edit',
        'query' => $destination,
      );
    }
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'admin/turbosms/messages/' . $node->sid . '/delete',
      'query' => $destination,
    );

    $options[$node->sid]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$node->sid]['operations'] = array(
        'data' => array(
          '#theme' => 'links__turbosms_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$node->sid]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  // Use a simple table.
  $form['nodes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Form builder; The form of sending SMS to the server TurboSMS.
 *
 * @ingroup forms
 * @see turbosms_admin_messages_send_submit() 
 */
function turbosms_admin_messages_send($form, &$form_state) {
  // Build the sortable table header.
  $header = array(
    'sid' => array('data' => t('ID'), 'field' => 'n.sid', 'sort' => 'desc'),
    'number' => array('data' => t('Number'), 'field' => 'n.number'),
    'sign' => array('data' => t('Sing'), 'field' => 'n.sign'),
    'message' => array('data' => t('Message'), 'field' => 'n.message'),
    'send_time' => array('data' => t('Send time'), 'field' => 'n.send_time'),
    'updated' => array('data' => t('Updated'), 'field' => 'n.updated'),
  );

  $query = db_select('turbosms', 'n')
    ->condition('dlr_status', NULL);

  $nids = $query
    ->fields('n', array('sid'))
    ->execute()
    ->fetchCol();

  if (count($nids) > 0) {
    $nodes = $query
      ->fields('n', array(
        'sid',
        'number',
        'sign',
        'message',
        'send_time',
        'updated',))
      ->condition('n.sid', $nids, 'IN')
      ->extend('PagerDefault')
      ->limit(20)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->addTag('turbosms_access')
      ->execute();

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send and check the status of SMS messages'),
      '#submit' => array('turbosms_admin_messages_send_submit'),
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Check the status of SMS messages'),
      '#submit' => array('turbosms_admin_messages_check_submit'),
    );

    $nodes = $query->execute();
  };

  $options = array();
  foreach ($nodes as $node) {
    $options[$node->sid] = array(
      'sid' => $node->sid,
      'number' => $node->number,
      'sign' => $node->sign,
      'message' => $node->message,
      'send_time' => ($node->send_time ? format_date($node->send_time, 'custom', 'Y-m-d h:i') : ''),
      'updated' => ($node->updated ? format_date($node->updated, 'custom', 'Y-m-d h:i') : ''),
    );
  }
  // Use a simple table.
  $form['nodes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Form submission handler for turbosms_admin_messages_send().
 *
 * @see turbosms_admin_messages_send()
 */
function turbosms_admin_messages_send_submit($form, &$form_state) {
  global $databases;
  $username = $databases['turbosms']['default']['username'];
  $sms_send = $form_state['complete form']['nodes']['#rows'];
  ksort($sms_send);
  $count = count($sms_send);

  for ($i = 1; $i <= $count; $i++) {
    $sid = array_shift($sms_send);
    _turbosms_transaction($sid, $username);
  }
  turbosms_admin_messages_check_submit($form, $form_state);
  cache_clear_all();
  $form_state['redirect'] = 'admin/turbosms/messages';
}

/**
 * Transaction.
 */
function _turbosms_transaction($sid, $table) {
  db_set_active('turbosms');
  $transaction = db_transaction();
  try {
    $id = db_insert($table)
      ->fields(array(
        'number' => $sid['number'],
        'sign' => $sid['sign'],
        'message' => $sid['message'],
        'send_time' => ($sid['send_time'] ? $sid['send_time'] : ''),
      ))
      ->execute();
    db_set_active('default');
    _turbosms_update_transaction($id);
    drupal_set_message(t('The send SMS id : ') . $id);
    return $id;
  }
  catch (Exception $e) {
    db_set_active('default');
    $transaction->rollback();
    watchdog_exception('turbosms', $e);
  }
}

/**
 * Update transaction.
 */
function _turbosms_update_transaction($sid) {
  if (!empty($sid)) {
    db_update('turbosms')
      ->condition('sid', $sid)
      ->fields(array('sid' => $sid, 'dlr_status' => 'UNSENDED'))
      ->execute();
  }
}

/**
 * Form submission handler for turbosms_admin_messages_send().
 *
 * @see turbosms_admin_messages_send()
 */
function turbosms_admin_messages_check_submit($form, &$form_state) {
  global $databases;
  $username = $databases['turbosms']['default']['username'];

  $nids = db_select('turbosms', 'n')
    ->fields('n', array('sid', 'dlr_status'))
    ->condition(
      db_or()
        ->condition('dlr_status', 'DELIVRD', '!=')
        ->condition('dlr_status', NULL, '!=')
      )
    ->execute()
    ->fetchCol();

  if (count($nids) > 0) {
    db_set_active('turbosms');
    $nodes = db_select($username, 'n')
      ->fields('n', array(
        'id',
        'msg_id',
        'cost',
        'credits',
        'send_time',
        'sended',
        'updated',
        'status',
        'dlr_status',
        ))
      ->condition('n.id', $nids, 'IN')
      ->addTag('turbosms_access')
      ->execute()
      ->fetchAll();
    db_set_active('default');
    $nodes_updated = db_select('turbosms', 'n')
      ->fields('n', array(
        'sid',
        'msg_id',
        'cost',
        'credits',
        'send_time',
        'sended',
        'updated',
        'status',
        'dlr_status',
        ))
      ->condition('n.sid', $nids, 'IN')
      ->addTag('turbosms_access')
      ->execute()
      ->fetchAll();

    foreach ($nodes as $key => $value) {
      foreach ($value as $k => $v) {
        switch ($k) {
          case 'send_time':
          case 'sended':
          case 'updated':
            $value->$k = ($v ? strtotime($v) : NULL);
            break;

        }
      }
      unset($nodes->$key);
      $nodes[$key] = (array) $value;
    }

    foreach ($nodes_updated as $key => $value) {
      unset($nodes_updated->$key);
      $nodes_updated[$key] = (array) $value;
    }

    foreach ($nodes as $key => $value) {
      foreach ($nodes_updated as $key_updated => $value_updated) {
        if ($value['id'] == $value_updated['sid']) {
          $updated = array_diff($value, $nodes_updated[$key_updated]);
          if (!empty($updated)) {
            $result[$key] = $updated;
          }
        }
      }
      if (!empty($updated)) {
        $result[$key]['sid'] = $value['id'];
      }
    }

    if (count($result) > 0) {
      $count_up = count($result);
      for ($j = 0; $j < $count_up; $j++) {
        $sid = array_shift($result);
        $rez = turbosms_update_check($sid);
        if (!empty($rez)) {
          drupal_set_message('SMS updated record with ID ' . $sid['sid']);
        }
      }
    }
  }
}
